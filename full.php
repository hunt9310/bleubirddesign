<!DOCTYPE html>
<html class="no-js" lang="">
    <head>
        <?=Loader::element('header_required'); ?>

        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="<?=$view->getThemePath()?>/css/normalize.css">
        <link rel="stylesheet" href="<?=$view->getThemePath()?>/css/main.css">
        <link rel="stylesheet" href="<?=$view->getThemePath()?>/css/bootstrap.min.css">


    </head>
    <body>
        <div class="<?=$c->getPageWrapperClass()?>">
            <!--[if lt IE 8]>
                <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
            <![endif]-->

            <!-- Add your site or application content here -->
            <div class="container-fluid">
                <? 
                 $a = new Area('Welcome');
                 $a->display($c);
                ?>
            </div>

           </div>

        <? Loader::element('footer_required'); ?>
    </body>
</html>
